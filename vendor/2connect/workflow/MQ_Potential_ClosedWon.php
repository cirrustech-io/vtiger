<?php
/*
 * Workflow Function 
 */


// composer autoloader required for AMQP libraries
require_once( 'vendor/autoload.php' );
use PhpAmqpLib\Connection\AMQPConnection;
use PhpAmqpLib\Message\AMQPMessage;

function MQ_Send_Potential( $entity ) 
{
	function doDebug( $data ) {
		$file = 'logs/workflow_MQ_Send_Potential.log';
		file_put_contents( $file, print_r($data, true), FILE_APPEND | LOCK_EX );
		file_put_contents( $file, "\n", FILE_APPEND | LOCK_EX );	
	} // end function doDebug
	
	function submitQueue( $json_data ) {
		$host = 'mq.2connectbahrain.com';
		$port = 5672;
		$username = 'guest';
		$password = 'guest';
		$vhost = 'crm';
		
		$exchange = 'cqb';
		$routingKey = 'workflow.new_account';
		
		$connection = new AMQPConnection($host, $port, $username, $password, $vhost);
		$channel = $connection->channel();
		
		#$channel->queue_declare('hello', false, false, false, false);
		
		$msg = new AMQPMessage($json_data, array('content_type' => 'application/json', 'delivery_mode' => 2));
		$channel->basic_publish($msg, $exchange, $routingKey);
		
		$channel->close();
		$connection->close();
		
		return true;
	} // end function submitQueue
	
	function getSalesOrders( $potential_id ) {
		/*
		Get list of active sales orders for the potential
		*/
		$moduleName = 'Potentials';
		$relatedModuleName = 'SalesOrder';
		$pagingModel = new Vtiger_Paging_Model();
	
		$parentRecordModel = Vtiger_Record_Model::getInstanceById($potential_id, $moduleName);
		$relationListView = Vtiger_RelationListView_Model::getInstance($parentRecordModel, $relatedModuleName );
		$models = $relationListView->getEntries($pagingModel);
		$noOfEntries = count($models);
		
		$salesOrders = Array();
		foreach ( $models as $key => $value ) {
			$thisSalesOrder = Vtiger_Record_Model::getInstanceById($key, $relatedModuleName)->getEntity()->column_fields;
			if( $thisSalesOrder['sostatus'] == 'Approved' ) {
				$salesOrders[] = $thisSalesOrder;
			}
		}
	
		$soCount = sizeof($salesOrders);

		// Only one active sales order should be present
		if( $soCount > 1 || $soCount == 0  ) {
			return false;
		}
		else {
			return $salesOrders[0];
		}
		
	} // end function getSalesOrders
	

	
	// Initialize master json 
	$json = new stdClass();
	$json->_appinfo = new stdClass();
	$json->_appinfo->application = 'MQ_Potential_ClosedWon.php';
	$json->_appinfo->script = Array(
		'name' => '',
		'description' => '',
		'version' => ''
	);
	$json->_appinfo->hostname = 'dev-01.i.2connectbahrain.com';
	$json->_appinfo->ip_address = '192.168.2.95';
	$json->_appinfo->mq = new stdClass();
	$json->_appinfo->mq = Array(
		'vhost' => '',
		'exchange' => '',
		'queue' => ''
	);
	$json->payload = new stdClass();
	$json->payload->account = Array();
	$json->payload->contact = Array();
	$json->payload->product = Array();
	$json->payload->salesorder = Array();

		
	// Potential Information
	$moduleName = $entity->moduleName;
	$potential_long_id = explode( 'x', $entity->id );
	$potId = $potential_long_id[1];
	$potData = $entity->data;

	// Get Account
	$accountModuleName = 'Accounts';
	$acctId = explode('x', $potData['related_to']);
	$account = Vtiger_Record_Model::getInstanceById( $acctId[1], 'Accounts' )->getEntity()->column_fields;
	$json->payload->account = $account;
	
	// Get Sales Order
	$json->payload->salesorder = getSalesOrders( $potId );
	
	if( $json->payload->salesorder ){
		// Get Product associated with Sales Order
		$product_id = $json->payload->salesorder['productid'];
		$json->payload->product = Vtiger_Record_Model::getInstanceById( $product_id, 'Products' )->getEntity()->column_fields;
		// Get Contact associated with Sales Order
		$contact_id = $json->payload->salesorder['contact_id'];
		$json->payload->contact = Vtiger_Record_Model::getInstanceById( $contact_id, 'Contacts' )->getEntity()->column_fields;
	}
	
	#doDebug( json_encode( $json ) );
	
	if( $json->payload->salesorder && submitQueue( json_encode( $json ) ) ) {
		$comment_message = 'MQ_Potential_ClosedWon.php Workflow Function was Triggered which sent a message in to the queue successfully.';
	} else {
		$comment_message = 'ERROR! SalesOrders=0 OR SalesOrders>1';
	}
	
	$comment = CRMEntity::getInstance('ModComments');
	$comment->column_fields['commentcontent'] = $comment_message;
	$comment->column_fields['related_to'] = $potId;
	#$comment->column_fields['userid'] = 5;
	$comment->save('ModComments');
	
	return true;

}

?>

