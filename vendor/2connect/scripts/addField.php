<?php

require_once("config.php");
include_once('vtlib/Vtiger/Module.php');
include_once('vtlib/Vtiger/Field.php');


/*
 * Add Subscriber ID for BRAS to Sales Order
 */
$moduleInstance = Vtiger_Module::getInstance('SalesOrder');
$fieldInstance = new Vtiger_Field();
$fieldInstance->label = 'Subscriber ID';
$fieldInstance->name = 'bras_subscriber_id';
$fieldInstance->table = $moduleInstance->basetable;
$fieldInstance->column = 'bras_subscriber_id';
$fieldInstance->columntype = 'VARCHAR(100)';
$blockInstance = Vtiger_Block::getInstance('LBL_CUSTOM_INFORMATION', $moduleInstance);
$blockInstance->addField( $fieldInstance );
echo "Added 'Subscriber ID' field to SalesOrder.";
echo "\n";


/*
 * Add QuickBooks ID field to all relevant entities
 */
$entities = Array(
	'SalesOrder',
	'Accounts',
	'Contacts',
	'Products',
	'Invoice',
);

foreach( $entities as $entity ) {
	$moduleInstance = Vtiger_Module::getInstance( $entity );
	$fieldInstance = new Vtiger_Field();
	$fieldInstance->label = 'QuickBooks ID';
	$fieldInstance->name = 'quickbooks_id';
	$fieldInstance->table = $moduleInstance->basetable;
	$fieldInstance->column = 'quickbooks_id';
	$fieldInstance->columntype = 'VARCHAR(19)';
	//$fieldInstance->masseditable = 0;
	$blockInstance = Vtiger_Block::getInstance('LBL_CUSTOM_INFORMATION', $moduleInstance);
	$blockInstance->addField( $fieldInstance );
	echo "Added 'QuickBooks ID' field to $entity.";
	echo "\n";
}


?>
